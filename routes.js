
const MainController = require("./controllers/MainController");

const Joi = require('joi');

module.exports = [
    {
        method: 'GET',
        path: '/{param*}',
        config: {
            auth: {
                mode: 'try',
                strategy: 'session',
            }
        },
        handler: {
            directory: {
                path: 'assets',
                redirectToSlash: false,
                index: false,
                listing: false
            }
        }
    },
    {
        method: 'GET',
        path: '/',
        config: {
            auth: {
                mode: 'try',
                strategy: 'session',
            }
        },
        handler: MainController.RenderIndexPage
    },

    {
        method: 'GET',
        path: '/contactus',
        config: {
            auth: {
                mode: 'try',
                strategy: 'session',
            }
        },
        handler: MainController.RenderContactPage
    },

    {
        method: 'GET',
        path: '/pricing',
        config: {
            auth: {
                mode: 'try',
                strategy: 'session',
            }
        },
        handler: MainController.RenderPricingPage
    },

    {
        method: 'GET',
        path: '/neworder',
        config: {
            auth: {
                mode: 'try',
                strategy: 'session',
            }
        },
        handler: MainController.RenderNewOrderPage
    },

    {
        method: 'GET',
        path: '/checkout',
        config: {
            auth: {
                mode: 'required',
                strategy: 'session',
            }
        },
        handler: MainController.RenderCheckoutPage
    },

    {
        method: 'POST',
        path: '/order/gotocheckout',
        config: {
            auth: {
                mode: 'try',
                strategy: 'session',
            },
            validate: {
                payload: {
                    type: Joi.string().required(),
                    title: Joi.string().required(),
                    numberOfPages: Joi.number().required(),
                    spacing: Joi.string().required(),
                    academicLevel: Joi.string().required(),
                    urgency: Joi.string().required(),
                    instructions: Joi.optional(),
                    numberOfReferences: Joi.number().required(),
                    style: Joi.string().required(),
                    languagePreference: Joi.string().required(),
                }
            }
        },
        handler: MainController.GoToCheckout
    },

    {
        method: 'POST',
        path: '/order/process',
        config: {
            auth: {
                mode: 'required',
                strategy: 'session',
            },
            validate: {
                payload: {
                    cardn: Joi.string().required(),
                    expmonth: Joi.number().required(),
                    expyear: Joi.number().required(),
                    cvc: Joi.string().required(),
                    name: Joi.string().required(),
                    email: Joi.string().email().required()
                }
            }
        },
        handler: MainController.ProcessOrder
    },

    {
        method: 'GET',
        path: '/success',
        config: {
            auth: {
                mode: 'required',
                strategy: 'session',
            }
        },
        handler: MainController.RenderSuccessOrder
    },

    {
        method: 'POST',
        path: '/order/estimateprice',
        config: {
            auth: {
                mode: 'try',
                strategy: 'session',
            },
            validate: {
                payload: {
                    numberOfPages: Joi.number().required(),
                    academicLevel: Joi.string().required(),
                    urgency: Joi.string().required(),
                    languagePreference: Joi.string().required(),
                }
            }
        },
        handler: MainController.CalculateEstimatePrice
    },

    {
        method: 'POST',
        path: '/order/upload',
        config: {
            auth: {
                mode: 'try',
                strategy: 'session',
            },
            payload: {
                output: 'file',
                parse: true,
                allow: 'multipart/form-data',
                maxBytes: 1e+9,
                timeout: false
            }
        },
        handler: MainController.UploadContentToMedia
    },
]
