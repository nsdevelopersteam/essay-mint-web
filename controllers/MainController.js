

const APIRequests           = require('../comunication/APIRequests.js');
const configuration         = require('../config');
const Boom                  = require('boom');
const Crypto                = require('crypto');
const HttpRequest           = require('request');
const fs                    = require('fs');
const currencyFormatter = require('currency-formatter');
const Q = require("q")

const arrMedia = new Array();

const stripeFlat = 0.30
const ukWriterPercentage = 0.05

function UploadContentToMediaRequest(filePathParam, mediaPathParam) {
    var Uploaddeferred = Q.defer();

    var formData = {
        document: fs.createReadStream(filePathParam.path),
    };

    HttpRequest.post({url: configuration.media.developmentURL + mediaPathParam, formData: formData}, function optionalCallback(err, httpResponse, body) {
        if(err) {
            console.log(err)
            Uploaddeferred.reject(err);
        }
        else {
            var obj = JSON.parse(body); 
            if(body === undefined) {
                Uploaddeferred.reject(obj);
            }
            else {
                if(obj.statusCode != 200) {
                    Uploaddeferred.reject(obj);
                }
                else {
                    Uploaddeferred.resolve(obj);
                }
            }
        }
    });
    return Uploaddeferred.promise;
}

module.exports = {
    RenderIndexPage: (request, reply) => {
        APIRequests.CountVisit();
        return reply.view('index', {}, { layout: 'DefaultLayout' });
    },

    RenderContactPage: (request, reply) => {
        return reply.view('contactus', {}, { layout: 'DefaultLayout' });
    },

    RenderPricingPage: (request, reply) => {
        return reply.view('pricing', {}, { layout: 'DefaultLayout' });
    },

    RenderNewOrderPage: (request, reply) => {
        request.cookieAuth.clear();
        return reply.view('neworder', {}, { layout: 'DefaultLayout' });
    },

    RenderCheckoutPage: (request, reply) => {
        
        var totalPrice = 0;
     
        totalPrice += configuration.orderTypes.basePrice    
        totalPrice += parseFloat(configuration.orderTypes.pricePerPage * request.auth.credentials.data.order.numberOfPages)
      
        var academicLevelByTypeArr = configuration.orderTypes.academicLevel
        var priceAcademicLevel = parseFloat(academicLevelByTypeArr.filter(obj => obj.level === request.auth.credentials.data.order.academicLevel)[0].price).toFixed(2)
        totalPrice+= parseFloat(priceAcademicLevel)
      
        var urgencyByTypeArr = configuration.orderTypes.urgency
        var priceUrgency = parseFloat(urgencyByTypeArr.filter(obj => obj.urgency === request.auth.credentials.data.order.urgency)[0].price).toFixed(2)
        totalPrice+= parseFloat(priceUrgency)

        var ukWriter = 0;
        if(request.auth.credentials.data.order.languagePreference === "uk-writer") {          
            ukWriter = parseFloat(ukWriterPercentage * parseFloat(totalPrice))
            totalPrice = parseFloat(totalPrice + parseFloat(ukWriter))   
        }

        var tax = parseFloat((0.039) * totalPrice) + parseFloat(stripeFlat)
        var totalPriceAfterTax = parseFloat(tax) + parseFloat(totalPrice) 

        var orderTotal = {
            subTotal: totalPriceAfterTax,
            tax: tax,
            total: totalPrice,
            order: request.auth.credentials.data.order,
            sid: request.auth.credentials.id,
        }; 

        request.server.app.cache.set(request.auth.credentials.id, orderTotal, 0, function(err) {
            request.cookieAuth.set({ sid: request.auth.credentials.id });
        })

        var data = {
            type: request.auth.credentials.data.order.type,
            totalPrice: currencyFormatter.format(totalPrice-ukWriter, { code: 'USD' }),
            numberOfPages: request.auth.credentials.data.order.numberOfPages,
            urgency: request.auth.credentials.data.order.urgency,
            tax: currencyFormatter.format(tax, { code: 'USD' }),
            subTotal : currencyFormatter.format(totalPriceAfterTax, { code: 'USD' }),
            ukWriter: currencyFormatter.format(ukWriter, { code: 'USD' }),
        }

        return reply.view('checkout', data, { layout: 'CheckoutLayout' });
    },

    GoToCheckout: (request, reply) => {
        
        var sessionID = Crypto.randomBytes(16).toString('hex');
        var newOrderData = {
            order: request.payload,
            sid: sessionID
        }; 
        
        try {
            request.server.app.cache.set(sessionID, newOrderData, 0, function(err) {
                if (err) {
                    console.log(err)
                    var infoResponse = {
                        statusCode: 500,
                        error: 'Internal Error',
                        message: 'An internal error occured, please try again later'
                    }
                    return reply(infoResponse)
                }
                else {
                    request.cookieAuth.set({ sid: sessionID });
                    reply({
                        statusCode: 200,
                        error: null,
                        message: 'success',
                        redirect: '/checkout'
                    })
                }
            });     
        } catch (error) {
            var infoResponse = {
                statusCode: 500,
                error: 'Internal Error',
                message: 'An internal error occured, please try again later'
            }
            return reply(infoResponse)   
        }
    },

    ProcessOrder: (request, reply) => {
        //TODO work on source list if any
        var sourceList = new Array();

        var description = "Order at essay-mint.net - " + request.auth.credentials.data.order.title
        APIRequests.ProcessPayment(request.payload.cardn, request.payload.expmonth, request.payload.expyear, request.payload.cvc, parseInt(request.auth.credentials.data.subTotal * 100), description)
        .then(paymentSucess => {
            APIRequests.ProcessOrder(request.auth.credentials.data.order.type, request.auth.credentials.data.order.title, request.auth.credentials.data.order.numberOfPages, request.auth.credentials.data.order.spacing, request.auth.credentials.data.order.academicLevel, request.auth.credentials.data.order.urgency, request.auth.credentials.data.order.instructions, arrMedia, request.auth.credentials.data.total, request.auth.credentials.data.tax, paymentSucess.paymentId, request.payload.name, request.payload.email, request.auth.credentials.data.order.style, request.auth.credentials.data.order.numberOfReferences, request.auth.credentials.data.order.languagePreference)
            .then(successOrderSaved => {

                var successOrder = {
                    orderId: successOrderSaved.orderId,
                    sid: request.auth.credentials.id
                }; 
        
                request.server.app.cache.set(request.auth.credentials.id, successOrder, 0, function(err) {
                    request.cookieAuth.set({ sid: request.auth.credentials.id });
                })

                reply({
                    statusCode: 200,
                    error: null,
                    message: 'success',
                    redirect: '/success'
                })
            })
            .catch(err => {
                console.log('here')
                console.log(err)
                reply(err)
            })
            
        })
        .catch(err => {
            console.log(err)
            reply(err)
        })
        
    },

    RenderSuccessOrder: (request, reply) => {
       
        var data = {
            orderId: request.auth.credentials.data.orderId
        }

        request.cookieAuth.clear();

        return reply.view('successOrder', data , { layout: 'DefaultLayout' });
    },

    CalculateEstimatePrice: (request, reply) => {
        var totalPrice = 0;
     
        totalPrice += configuration.orderTypes.basePrice    
        totalPrice += parseFloat(configuration.orderTypes.pricePerPage * request.payload.numberOfPages)
      
        var academicLevelByTypeArr = configuration.orderTypes.academicLevel
        var priceAcademicLevel = parseFloat(academicLevelByTypeArr.filter(obj => obj.level === request.payload.academicLevel)[0].price).toFixed(2)
        totalPrice+= parseFloat(priceAcademicLevel)
      
        var urgencyByTypeArr = configuration.orderTypes.urgency
        var priceUrgency = parseFloat(urgencyByTypeArr.filter(obj => obj.urgency === request.payload.urgency)[0].price).toFixed(2)
        totalPrice+= parseFloat(priceUrgency)

        var ukWriter = 0;
        if(request.payload.languagePreference === "uk-writer") {          
            ukWriter = parseFloat(ukWriterPercentage * parseFloat(totalPrice))
            totalPrice = parseFloat(totalPrice + parseFloat(ukWriter))   
        }

        reply({
            statusCode: 200,
            error: null,
            message: 'success',
            total: totalPrice.toFixed(2)
        })
    },

    UploadContentToMedia: function(request, reply) {
        try {      
            UploadContentToMediaRequest(request.payload.file, "/file/post")
            .then(succesUpload => {

                var media = {
                    path: succesUpload.fileName,
                }
                
                arrMedia.push(media)
                
                return reply({
                    statusCode: 200,
                    message: 'success',
                    error: null,
                })
            }) 
            .catch(error => {
                console.log(error)
                return reply({
                    statusCode: 500,
                    message: "Ocorreu um erro interno, por favor tente mais tarde",
                    error: "Internal Error"
                })  
            })   
     
        } catch (error) {
            console.log(error)
            return reply({
                statusCode: 500,
                message: "Ocorreu um erro interno, por favor tente mais tarde",
                error: "Internal Error"
            })  
        }
    },
}
