const hapi                  = require('hapi');
const Hoek                  = require('hoek');
const routes                = require('./routes');
const Path                  = require('path');
const Inert                 = require('inert');
var CatboxRedis             = require('catbox-redis');

var AuthCookie  = require('hapi-auth-cookie');

const configuration         = require('./config');

// Create hapi server instance
/*var server = new hapi.Server({
    cache: {
        engine: CatboxRedis,
        host: "localhost",
        port: 6379,
    },
});*/
var server = new hapi.Server({
    cache: {
        engine: CatboxRedis,
        host: "redis.kiambote.com",
        port: 6379,
        password: '4334950390sasd9209432904was09d90asd0239490230d9sa90das90e90230492309ewasz09d09qe402309wsaadDDSDSDASD234234BGVJHNBUJLJHRDDAadfsd'
    },
});

// add connection parameters
server.connection({
    port: 7061,
    routes: {
        files: {
            relativeTo: Path.join(__dirname, 'views')
        }
    },

});

var happiError = require('hapi-error');
const configErrors = {
  statusCodes: {
    500: {
        message: 'An internal error occured',
        errorTitle: 'Error'
    },
    404: {
        message: 'Page not Found',
        errorTitle: 'Error'
    }
  }
};

happiError.options = configErrors;
server.register([happiError, require('vision'), AuthCookie], (err) => {

    Hoek.assert(!err, err);

    server.app.cache = server.cache({
        segment: 'sessions',
        expiresIn: 1800000
    });

    server.auth.strategy('session','cookie',{
        cookie: 'example',
        password: 'password-should-be-32-characters',
        isSecure: false, // For development only
        redirectTo: '/',
        redirectOnTry: false,
        appendNext: 'redirect',
        validateFunc: function(request, session, callback) {

            // Use session id (from cookie) to get session data (from cache).
            // If successful session id and data will be exposed in request.auth.credentials.
            request.server.app.cache.get(session.sid, function(err, value, cached, report) {
                var creds = {
                    id: session.sid,
                    data: value,
                };

                if (err) {
                    return callback(err, false);
                }

                if (!cached) {
                    return callback(null, false);
                }

                return callback(null, true, creds);
            });
        },
    });

    server.views({
        engines: {
            html: {
                module: require('handlebars'),
                compileMode: 'sync' // engine specific
            }
        },
        compileMode: 'sync',
        path: 'views',
        relativeTo: __dirname,
        layoutPath: 'views/layout',
        helpersPath: 'views/helpers',
        partialsPath: 'views/partials'
    });

    server.route(routes);
});

//registering
server.register(Inert, () => {});

server.start(function(){
    console.log('Now Visit: http://127.0.0.1:'+7061);
}); // uncomment this to run the server directly
