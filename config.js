module.exports = {
    orderTypes: 
    {
        type: "essay",
        basePrice: 14.99,
        pricePerPage: 0.49,
        academicLevel: [
            {
                level: "high-school",
                price: 2.99
            },
            {
                level: "college",
                price: 3.99
            },
            {
                level: "university",
                price: 4.99
            },
            {
                level: "master",
                price: 5.99
            },
            {
                level: "phd",
                price: 6.99
            }
        ],
        urgency: [
            {
                urgency: "3-hours",
                price: 29.99
            },
            {
                urgency: "6-hours",
                price: 27.99
            },
            {
                urgency: "8-hours",
                price: 25.99
            },
            {
                urgency: "12-hours",
                price: 22.99
            },
            {
                urgency: "24-hours",
                price: 19.99
            },
            {
                urgency: "48-hours",
                price: 17.99
            },
            {
                urgency: "3-days",
                price: 16.99
            },
            {
                urgency: "4-days",
                price: 15.99
            },
            {
                urgency: "5-days",
                price: 14.99
            },
            {
                urgency: "6-days",
                price: 13.99
            },
            {
                urgency: "7-days",
                price: 12.99
            },
            {
                urgency: "8-days",
                price: 12.99
            },
            {
                urgency: "9-days",
                price: 12.99
            },
            {
                urgency: "10-days",
                price: 12.99
            }
        ]
    },

    API: {
        developmentURL: 'http://localhost:7060',
        stagingURL: 'https://apiessaymint.nossapps.com',
        productionURL: 'https://api.kiambote.com',
        key: '345634252492049028459294910950205912934DFHE244',
        fakeJwt: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhMGYxNGIzNjM0OTgxMzNiNjFlMTIwOSIsImVtYWlsIjoiZXZhbmlsc29uYnJhdWxpb0BnbWFpbC5jb20iLCJpYXQiOjE1MTA5Mzc4MzIsImV4cCI6MTUxMDk1NTgzMn0.n62zTX2Lag2bXlXLhoVLqD9sFpYNMlPiUCar_BjrM0Q'
    },
    media: {
        stagingURL : "http://essaymintmedia.nossapps.com",
        developmentURL: 'http://localhost:7062',
        productionURL: 'https://media.essay-mint.net',
        accessKey: '1101029293838474756565567447833892920101'
    },
}   