const configuration               = require('../config');
const Client                      = require('node-rest-client').Client;
const Q                           = require('q');

var options = {
    connection: {
        rejectUnauthorized: false,
    },
    requestConfig: {
        timeout: 60000,
        noDelay: true,
        keepAlive: true,
        keepAliveDelay: 1000
    },
    responseConfig: {
        timeout: 300000
    }
};

var client = new Client(options);

module.exports = {
    ProcessPayment: function(cardNParam, expMonthParam, expYearParam, cvvParam, amountParam, descriptionParam) {
        var deferred = Q.defer();
        var args = {
            headers: { 
                "Content-Type": "application/json",
            },
            data: {
                "cardn": cardNParam,
                "expmonth": expMonthParam,
                "expyear": expYearParam,
                "cvv": cvvParam,
                "amount": amountParam,
                "description": descriptionParam     
            }
        };
        client.post(configuration.API.developmentURL + "/order/pay", args, function (dataBasicInfo, response) {
            if(dataBasicInfo['statusCode'] == 200) {
                deferred.resolve(dataBasicInfo);
            }
            else {
                deferred.reject(dataBasicInfo);
            }
        })
        
        return deferred.promise;
    },

    ProcessOrder: function(typeParam, titleParam, numberOfPagesParam, spacingParam, academicLevelParam, urgencyParam, notesParam, sourceListParam, totalParam, feeParam, paymentConfirmationParam, customerNameParam, customerEmailParam, styleParam, numberOfReferencesParam, languagePreferenceParam) {
        var deferred = Q.defer();
        var args = {
            headers: { 
                "Content-Type": "application/json",
            },
            data: {
                "type": typeParam,
                "title": titleParam,
                "numberOfPages": numberOfPagesParam,
                "spacing": spacingParam,
                "academicLevel": academicLevelParam,
                "urgency": urgencyParam,
                "notes": notesParam,
                "sourcesList": sourceListParam,
                "total": totalParam,
                "fee": feeParam,
                "paymentConfirmationId": paymentConfirmationParam,
                "customerName": customerNameParam,
                "customerEmail": customerEmailParam,
                "style": styleParam,
                "languageStyle": languagePreferenceParam,
                "numberOfSources": numberOfReferencesParam, 
            }
        };
        client.post(configuration.API.developmentURL + "/order/process", args, function (dataBasicInfo, response) {
            if(dataBasicInfo['statusCode'] == 200) {
                deferred.resolve(dataBasicInfo);
            }
            else {
                deferred.reject(dataBasicInfo);
            }
        })
        
        return deferred.promise;
    },

    CountVisit: function() {
        var deferred = Q.defer();
        var args = {
            headers: { 
                "Content-Type": "application/json",
            },
        };
        client.get(configuration.API.developmentURL + "/guest/countvisit", args, function (dataBasicInfo, response) {
            if(dataBasicInfo['statusCode'] == 200) {
                deferred.resolve(dataBasicInfo);
            }
            else {
                deferred.reject(dataBasicInfo);
            }
        })
        
        return deferred.promise;
    },
}